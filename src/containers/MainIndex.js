import React from 'react';
import {Component} from 'react';

import Filter from '../components/Filter';
import FlatsIndex from '../components/FlatList';
import Pagination from '../components/Pagination';
import styles from '../css/style.css';
import sidebar from '../css/simple-sidebar.css';

export default class MainIndex extends Component {
  render()
  {
    return (
      <div>
		<div id='wrapper' className='toggled' style={{marginTop: '50px'}}>
        <Filter/>
		<FlatsIndex/>
        <div className='flat-footer'>
			<Pagination/>
		</div>
		</div>
      </div>
    );
  }
}