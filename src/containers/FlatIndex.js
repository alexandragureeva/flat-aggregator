import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchFlat, getCities, getSubways, clearFlat } from '../actions/index';
import FlatGallery from '../components/FlatGallery';
import styles from '../css/flatStyle.css';

import $ from 'jquery';

class FlatIndex extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentWillMount() {
        this.props.getCities();
        this.props.getSubways();
        this.props.fetchFlat(this.props.params.id);
    }

    renderMap(flat) {
        ymaps.ready(init);
        var myMap, myPlacemark;

        function init() {
            console.log('in ymaps');
            var myGeocoder = ymaps.geocode(flat.city + ' ' + flat.address);
            myGeocoder.then(function (res) {
                myMap = new ymaps.Map('map', {
                    center: res.geoObjects.get(0).geometry.getCoordinates(),
                    zoom: 15
                });
                myMap.geoObjects.add(res.geoObjects);
            }, function (err) {
                myMap = new ymaps.Map('map', {
                    center: [55.76, 37.64],
                    zoom: 15
                });
                myPlacemark = new ymaps.Placemark([55.76, 37.64], {
                    hintContent: 'Москва',
                    balloonContent: 'Столица России'
                });
                myMap.geoObjects.add(myPlacemark);
            });
        }
    }

    handleAccordion(e) {
        e.target.classList.toggle('active');
        e.target.nextElementSibling.classList.toggle('show');
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('Component did update');
        if (this.props.flat) {
            this.renderMap(this.props.flat);
            console.log(document.getElementsByName('.ymaps-2-1-45-map').length);
            $('.ymaps-2-1-45-map').remove();
        }
    }

    componentWillUnmount() {
        this.props.clearFlat();
    }

    renderComments() {
        console.log('in comments')
        if (this.props.flat.comments) {
            return this.props.flat.comments.map((comment, i) => {
                    return ( <div className = 'card' key = { i }>
                                <div className = 'card-header' >
                                    { comment.date }
                                </div>
                                <div className = 'card-block'>
                                    <p className = 'card-text' >
                                        { comment.text }
                                    </p> 
                                </div>
                            </div>
                );
            });
        } else {
            return ( <div> Comments loading.... </div>);
            }
        }

    renderHistory() {
        console.log('in history')
        if (this.props.flat.histories) {
            return this.props.flat.histories.map((history, i) => {
                    return ( <div className = 'card' key = { i }> 
                                <div className = 'card-header' > 
                                    { history.start } - { history.end } 
                                </div>
                                <div className = 'card-block' >
                                    <p className = 'card-text'>
                                        { history.website }: was { history.status }.
                                    </p>
                                    <p className = 'card-text' >
                                        Price: { history.price } rub 
                                    </p>
                                </div>
                            </div>
                        );
            });
        } else {
            return ( <div> History loading.... </div>);
            }
        }

renderFlatType() {
    switch (this.props.flat.rooms) {
        case 1:
            return ( <p> Type of the flat: 1 - room flat </p>);
        case 2:
            return ( <p> Type of the flat: 2 - room flat </p>);
        case 3:
            return ( <p> Type of the flat: 3 - room flat </p>);
        case 4:
            return ( <p> Type of the flat: 4 - room flat </p>);
        case 5:
            return ( <p> Type of the flat: 5 - room flat </p>);
        case 6:
            return ( <p> Type of the flat: 6 - room flat </p>);
        case 7:
            return ( <p> Type of the flat: studio </p>);
        case 8:
            return ( <p> Type of the flat: free plan </p>);
    }
}
                    
render() {
    var citySubways = [];
    var city = '';
    if (this.props.flat) {
        if (this.props.subways) {
            for (var i = 0; i < this.props.subways.length; i++) {
                if (+this.props.subways[i].cityId === +this.props.flat.city) {
                    for (var j = 0; j < this.props.subways[i].subways.length; j++) {
                        citySubways.push(this.props.subways[i].subways[j].name);
                    }
                    break;
                }
            }
        }
        if (this.props.cities) {
            for (var i = 0; i < this.props.cities.length; i++) {
                if (+this.props.cities[i].id === +this.props.flat.city) {
                    city = this.props.cities[i].city;
                    break;
                }
            }
        }
        return (
            <div>
                <div className='row flatRow'>
                    <div className='col-lg-6'>
                        <FlatGallery photos={ this.props.flat.photos } />
                    </div>
                    <div className='col-lg-6 info'>
                        <h4 className='flath4'> General information</h4>
                        <div className='container'>
                            <p style={{ fontSize: '22px', margin: '1%' }}> Flat address: { city + ', ' + this.props.flat.address }</p>
                            { this.renderFlatType() }
                            { this.props.flat.subway == 0 ? '' : < p> Underground station: { citySubways[this.props.flat.subway - 1] }</p>}
                            <br/>
                            <p style={{ fontWeight: 'bold', fontSize: '18px' }}> { this.props.flat.price } rub/month </p>
                        </div>
                        <div className='other-information'>
                            <label></label>
                        </div>
                    </div>
                </div>
                <div className='row flatRow' style={{ marginTop: '2%' }}>
                    <div className='col-lg-5 sites'>
                        <h4 className='flath4'> Additional information </h4>
                        <button className='accordion' onClick={ this.handleAccordion }> Avito </button>
                        <div className='accord'>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                        </div>
                        <button className='accordion' onClick={ this.handleAccordion }> CIAN </button>
                        <div className='accord'>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                        </div>
                        <button className='accordion' onClick={ this.handleAccordion }> Yandex </button>                                        <div className='accord'>
                                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                    </div>
                    <div className='map'>                                                                                                      <h4 className='flath4'> Map </h4>
                         <div id='map' style={{ width: '100%', height: '90%' }}></div>
                    </div>
                </div>
                <div className='row flatRow' style={{ marginTop: '2%' }}>
                    <div className='comments'>
                        <h4 className='flath4'> Comments </h4>
                        { this.renderComments() }
                    </div>
                    <div className='col-lg-5 flat-history'>
                        <h4 className='flath4'> History  </h4>
                        { this.renderHistory() }
                    </div>
                </div>
            </div>
        );
    } else {
        return ( <div></div>);
        }
    }
}

function mapStateToProps(state) {
    return {
        flat: state.info.flat,
        subways: state.info.rusSubways,
        cities: state.info.cities
    };
}

export
default connect(mapStateToProps, {fetchFlat, getCities, getSubways, clearFlat })(FlatIndex);