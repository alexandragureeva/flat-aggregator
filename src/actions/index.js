import axios from 'axios';

export const GET_CITIES = 'GET_CITIES';
export const GET_FLATS = 'GET_FLATS';
export const CLEAR_FLATS = 'CLEAR_FLATS';
export const GET_FLAT = 'GET_FLAT';
export const CLEAR_FLAT = 'CLEAR_FLAT';
export const CHANGE_ORDER = 'CHANGE_ORDER';
export const APPLY_FILTER = 'APPLY_FILTER';
export const CHANGE_PAGE = 'CHANGE_PAGE';
export const CHANGE_ROOMNUMBER = 'CHANGE_ROOMNUMBER';
export const GET_SUBWAYS = 'GET_SUBWAYS';
const ROOT_URL = 'http://localhost:8081';

export function changeOrder(order) {
    return {
        type: CHANGE_ORDER,
        payload: order
    }
}

export function changeRoomNumber(rooms) {
    return {
        type: CHANGE_ROOMNUMBER,
        payload: rooms
    }
}

export function changePage(page) {
    return {
        type: CHANGE_PAGE,
        payload: page
    }
}

export function applyFilter(filters) {
    return {
        type: APPLY_FILTER,
        payload: filters
    }
}

export function getCities() {
    const request = axios.get(`${ROOT_URL}/cities`);

    console.log(request);

    return {
        type: GET_CITIES,
        payload: request
    }
}

export function getSubways() {
    const request = axios.get(`${ROOT_URL}/subways`);


    return {
        type: GET_SUBWAYS,
        payload: request
    }
}


export function getFlats(props) {
    console.log(props);
    const request = axios.post(`${ROOT_URL}/getFlats`,
        props, {
            headers: {
                "content-type": "application/json"
            }
        });

    console.log(request);
    return {
        type: GET_FLATS,
        payload: request
    }
}

export function fetchFlat(id) {
    const request = axios.get(`${ROOT_URL}/getFlat/` + id);
    return {
        type: GET_FLAT,
        payload: request
    }
}

export function clearFlat() {
    return {
        type: CLEAR_FLAT
    }
}

export function clearFlats() {
    return {
        type: CLEAR_FLATS
    }
}