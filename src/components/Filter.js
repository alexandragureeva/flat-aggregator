import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { getCities, getSubways, getFlats, applyFilter, changePage } from '../actions/index';
import Rooms from '../components/RoomNumber';

class Filter extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.changeSelect = this.changeSelect.bind(this);
        this.state = {
            chosenCity: 0
        };
    }
    componentWillMount() {
        this.props.getCities();
        this.props.getSubways();
    }

    renderCitySelect() {
        return this.props.cities.map((city) => {
            return ( 
                <option value = { city.id } key = { city.id}> { city.city} </option>
            );
        });
    }

    componentWillUpdate() {
        if (ReactDOM.findDOMNode(this.refs.inputCity) !== null) {
            this.state.chosenCity = +ReactDOM.findDOMNode(this.refs.inputCity).value;
        } else {
            this.state.chosenCity = '';
        }
    }

    renderSubwaySelect() {
        if (this.state.chosenCity !== '') {
            var rusSubways = this.props.subways;
            for (var i = 0; i < rusSubways.length; i++) {
                if (rusSubways[i].cityId === this.state.chosenCity) {
                    return rusSubways[i].subways.map((subway) => {
                        return ( <option value = { subway.id } key = { subway.id } > { subway.name } < /option>
                        );
                    });
                }
            }
        }
    }

    changeSelect() {
        ReactDOM.findDOMNode(this.refs.inputSubway).value = 0;
        var addressToApply = ReactDOM.findDOMNode(this.refs.inputAddress).value;
        var cityToApply = +ReactDOM.findDOMNode(this.refs.inputCity).value;
        var minMetersToApply = +ReactDOM.findDOMNode(this.refs.inputMinMeters).value;
        var maxMetersToApply = +ReactDOM.findDOMNode(this.refs.inputMaxMeters).value;
        var subwayToApply = +ReactDOM.findDOMNode(this.refs.inputSubway).value;
        var minPriceToApply = +ReactDOM.findDOMNode(this.refs.inputMinPrice).value;
        var maxPriceToApply = +ReactDOM.findDOMNode(this.refs.inputMaxPrice).value;
        this.props.applyFilter({
            city: this.props.city,
            address: addressToApply,
            subway: subwayToApply,
            meters: [minMetersToApply, maxMetersToApply],
            price: [minPriceToApply, maxPriceToApply]
        });
    }

    onSubmit() {
        this.props.changePage(0);
        var addressToApply = ReactDOM.findDOMNode(this.refs.inputAddress).value;
        var cityToApply = +ReactDOM.findDOMNode(this.refs.inputCity).value;
        var subwayToApply = +ReactDOM.findDOMNode(this.refs.inputSubway).value;
        var minMetersToApply = +ReactDOM.findDOMNode(this.refs.inputMinMeters).value;
        var maxMetersToApply = +ReactDOM.findDOMNode(this.refs.inputMaxMeters).value;
        var minPriceToApply = +ReactDOM.findDOMNode(this.refs.inputMinPrice).value;
        var maxPriceToApply = +ReactDOM.findDOMNode(this.refs.inputMaxPrice).value;
        this.props.applyFilter({
            city: cityToApply,
            address: addressToApply,
            subway: subwayToApply,
            meters: [minMetersToApply, maxMetersToApply],
            price: [minPriceToApply, maxPriceToApply]
        });
        this.props.getFlats({
            filters: {
                city: cityToApply,
                subway: subwayToApply,
                address: addressToApply,
                order: this.props.order,
                rooms: this.props.rooms,
                meters: [minMetersToApply, maxMetersToApply],
                price: [minPriceToApply, maxPriceToApply],
                flatOnPage: this.props.flatOnPage,
                pageNumber: 1
            }
        });
    }


    render() {
        return (
            <div id = 'sidebar-wrapper' >
                <ul className = 'sidebar-nav' >
                    <div className = 'cd-filter' >
                        <div >
                            <li className = 'sidebar-brand' >
                                <div className = 'cd-header' >
                                    <h1> Filters </h1>
                                </div>
                            </li>
                            <li>
                                <div className = 'cd-filter-block'>
                                    <h4> Address </h4>
                                    <input type = 'search' id = 'address' name = 'address' placeholder = 'Enter address here...' defaultValue = '' ref = 'inputAddress' />
                                </div> 
                            </li > 
                            <li >
                                <div className = 'cd-filter-block' >
                                    <h4> City </h4>
                                    <div className = 'cd-select cd-filters'>
                                        <select className = 'filter' onChange = { this.changeSelect } name = 'city' id = 'city' defaultValue = { this.props.city } ref = 'inputCity' > 
                                            { this.renderCitySelect() } 
                                        </select>
                                    </div>
                                </div>
                            </li >
                            <li>
                                <div className = 'cd-filter-block' >
                                    <h4> Price </h4>
                                    <label> From: </label > 
                                    <input type = 'number' id = 'minPrice' defaultValue = '' ref = 'inputMinPrice' />
                                    <label> To: </label> 
                                    <input type = 'number' id = 'maxPrice' defaultValue = '' ref = 'inputMaxPrice' />
                                </div> 
                            </li >
                            <li >
                                <div className = 'cd-filter-block' >
                                    <h4> Square meters </h4>
                                    <label> From: </label>
                                    <input type = 'number' id = 'minMeters' defaultValue = '' ref = 'inputMinMeters' />
                                    <label> To: </label> 
                                    <input type = 'number' id = 'maxMeters' defaultValue = '' ref = 'inputMaxMeters' />
                                </div> 
                            </li > 
                            <li >
                                <div className = 'cd-filter-block' >
                                    <h4> Subway station </h4> 
                                    <div className = 'cd-select cd-filters'>
                                        <select id = 'subway' defaultValue = { this.props.subway } ref = 'inputSubway'>
                                            <option value = '0' key = '0'> Not chosen </option> 
                                            { this.renderSubwaySelect() } 
                                        </select> 
                                    </div > 
                                </div> 
                            </li > 
                            <li >
                                <div className = 'cd-filter-block' >
                                    <h4> Room number </h4>
                                    <Rooms / >
                                </div> 
                            </li > 
                            <li >
                                <button className = 'cd-close' onClick = { this.onSubmit }> Apply filters </button>
                            </li>
                        </div>
                    </div >
                </ul>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        cities: state.info.cities,
        city: state.info.filters.city,
        subway: state.info.filters.subway,
        subways: state.info.rusSubways,
        address: state.info.filters.address,
        order: state.info.order,
        rooms: state.info.rooms,
        price: state.info.filters.price,
        meters: state.info.filters.meters,
        flatOnPage: state.info.flatOnPage
    };
}

export default connect(mapStateToProps, 
                       { getCities, getSubways, getFlats, applyFilter, changePage })(Filter);