import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { changeOrder, getFlats, changePage } from '../actions/index';

class PriceOrder extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) {
        this.props.changePage(0);
        var order = (e.target.id === 'cheap') ? 0 : 1;
        this.props.changeOrder(order);
        this.props.getFlats({
            filters: {
                city: this.props.city,
                subway: this.props.subway,
                address: this.props.address,
                price: this.props.price,
                meters: this.props.meters,
                order: order,
                rooms: this.props.rooms,
                flatOnPage: this.props.flatOnPage,
                pageNumber: 1
            }
        });

    }

    render() {
        return ( <div className = 'row'>
                    <div className = 'btn-group priceOrder' >
                        <a href = '#' id = 'cheap' onClick = { this.onSubmit } className = { 'btn fil-btn ' + (this.props.order === 0 ? 'active' : '') }> Cheaper 
                        </a>
                        <a id = 'exp' onClick = { this.onSubmit } className = { 'btn fil-btn ' + (this.props.order === 1 ? 'active' : '') } > More Expensive 
                        </a>  
                    </div>
                </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        city: state.info.filters.city,
        address: state.info.filters.address,
        subway: state.info.filters.subway,
        order: state.info.order,
        rooms: state.info.rooms,
        price: state.info.filters.price,
        meters: state.info.filters.meters,
        flatOnPage: state.info.flatOnPage

    };
}

export default connect(mapStateToProps, { getFlats, changeOrder, changePage })(PriceOrder);