import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { changeRoomNumber, getFlats, changePage } from '../actions/index';

class Rooms extends Component {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(e) {
        this.props.changePage(0);
        var newRooms = this.props.rooms;
        var roomNumber;
        var isActive = false;
        switch (e.target.id) {
        case 'room1':
            roomNumber = 1;
            isActive = (newRooms.indexOf(1) === -1);
            break;
        case 'room2':
            roomNumber = 2;
            isActive = (newRooms.indexOf(2) === -1);
            break;
        case 'room3':
            roomNumber = 3;
            isActive = (newRooms.indexOf(3) === -1);
            break;
        case 'room4':
            roomNumber = 4;
            isActive = (newRooms.indexOf(4) === -1);
            break;
        case 'room5':
            roomNumber = 5;
            isActive = (newRooms.indexOf(5) === -1);
            break;
        case 'room6':
            roomNumber = 6;
            isActive = (newRooms.indexOf(6) === -1);
            break;
        case 'studio':
            roomNumber = 7;
            isActive = (newRooms.indexOf(7) === -1);
            break;
        case 'free':
            roomNumber = 8;
            isActive = (newRooms.indexOf(8) === -1);
            break;
        }
        if (isActive) {
            newRooms.push(roomNumber);
        } else {
            var itsIndex = newRooms.indexOf(roomNumber);
            newRooms.splice(itsIndex, 1);
        }
        this.props.changeRoomNumber(newRooms);
        this.setState({
            rooms: newRooms
        });
        this.props.getFlats({
            filters: {
                city: this.props.city,
                subway: this.props.subway,
                address: this.props.address,
                price: this.props.price,
                meters: this.props.meters,
                order: this.props.order,
                rooms: newRooms,
                flatOnPage: this.props.flatOnPage,
                pageNumber: 1
            }
        });
    }

    render() {
        return ( <div className = 'btn-group roomNumbers' >
                    <a href = '#' id = 'room1' onClick = { this.onSubmit } className = { 'btn fil-btn ' +(this.props.rooms.indexOf(1) != -1 ? 'active' : '') }> 1 </a> 
                    <a href = '#' id = 'room2' onClick = { this.onSubmit } className = { 'btn fil-btn ' + (this.props.rooms.indexOf(2) != -1 ? 'active' : '') }> 2 </a>
                    <a href = '#' id = 'room3' onClick = { this.onSubmit } className = { 'btn fil-btn ' + (this.props.rooms.indexOf(3) !== -1 ? 'active' : '') }> 3 </a>
                    <a href = '#' id = 'room4' onClick = { this.onSubmit } className = { 'btn fil-btn ' + (this.props.rooms.indexOf(4) !== -1 ? 'active' : '') }> 4 </a>
                    <a href = '#' id = 'room5' onClick = { this.onSubmit } className = { 'btn fil-btn ' + (this.props.rooms.indexOf(5) !== -1 ? 'active' : '') }> 5 </a>
                    <a href = '#' id = 'room6' onClick = { this.onSubmit } className = { 'btn fil-btn ' + (this.props.rooms.indexOf(6) !== -1 ? 'active' : '') }> 6 </a>
                    <a href = '#' id = 'studio' onClick = { this.onSubmit } className = { 'btn fil-btn ' + (this.props.rooms.indexOf(7) !== -1 ? 'active' : '') }> studio </a>
                    <a href = '#' id = 'free' onClick = { this.onSubmit } className = { 'btn fil-btn ' + (this.props.rooms.indexOf(8) !== -1 ? 'active' : '') }> free plan </a>
                </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        city: state.info.filters.city,
        address: state.info.filters.address,
        subway: state.info.filters.subway,
        price: state.info.filters.price,
        meters: state.info.filters.meters,
        order: state.info.order,
        rooms: state.info.rooms,
        flatOnPage: state.info.flatOnPage

    };
}

export default connect(mapStateToProps, { getFlats, changeRoomNumber, changePage })(Rooms);