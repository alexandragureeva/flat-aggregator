import React, { Component } from 'react';
import { connect } from 'react-redux';
import styles from '../css/flatStyle.css';

class FlatGallery extends Component {
    constructor(props) {
        super(props);
    }

    renderImages() {
        return this.props.photos.map((photo, i) => {
            return ( <div className = { 'item' + (i == 0 ? ' active' : '') } key = { i }>
                                     <img alt = '' title = '' src = { photo }/></div>
            );
        });
    }

    renderImagesIcons() {
        return this.props.photos.map((photo, i) => {
            return ( <li data-target = '#article-photo-carousel' data-slide-to = { i } key = { i } className = { i == 0 ? 'active' : '' }> <img alt = '' src = { photo }/> < /li>
            );
        });
    }

    render() {
        if (this.props.photos) {
            return ( <div className = 'carousel slide article-slide' id = 'article-photo-carousel'>
                        <div className = 'carousel-inner cont-slider'> 
                            { this.renderImages() } 
                        </div>
                        <ol className = 'carousel-indicators'>
                            { this.renderImagesIcons() }
                        </ol> 
                    </div>
            );
        } else {
            return ( <h1> Loading... </h1>);
            }
        }
    }

    function mapStateToProps(state) {
        return {};
    }

    export default connect(mapStateToProps, {})(FlatGallery);