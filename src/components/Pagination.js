import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { changePage, getFlats } from '../actions/index';
import Paginate from 'react-pagination-component';

class Pagination extends Component {
    constructor(props) {
        super(props);
        this.changePageNumber = this.changePageNumber.bind(this);
    }


    changePageNumber(page) {
        this.props.changePage(page);
        this.setState({
            pageNumber: page
        });
        this.props.getFlats({
            filters: {
                city: this.props.city,
                subway: this.props.subway,
                address: this.props.address,
                price: this.props.price,
                meters: this.props.meters,
                order: this.props.order,
                rooms: this.props.rooms,
                flatOnPage: this.props.flatOnPage,
                pageNumber: page
            }
        });
    }

    render() {
        console.log('in pagination' + this.props.pageNumber);
        return ( <Paginate totalPage = { this.props.pageCount } currentPage = { this.props.pageNumber } focusPage = {      this.changePageNumber }/>
        );
    }
}


function mapStateToProps(state) {
    return {
        city: state.info.filters.city,
        address: state.info.filters.address,
        subway: state.info.filters.subway,
        price: state.info.filters.price,
        meters: state.info.filters.meters,
        order: state.info.order,
        rooms: state.info.rooms,
        pageCount: state.info.pageCount,
        pageNumber: state.info.pageNumber,
        flatOnPage: state.info.flatOnPage
    };
}

export default connect(mapStateToProps, { getFlats, changePage })(Pagination);