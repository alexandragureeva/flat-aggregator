import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getFlats, clearFlats } from '../actions/index';
import { Link } from 'react-router';
import PriceOrder from '../components/PriceOrder';

import $ from 'jquery';

class FlatsIndex extends Component {

    constructor(props) {
        super(props);
        this.toggleFilters = this.toggleFilters.bind(this);
    }
    componentWillMount() {
        var json = this.createFilterJson();
        this.props.getFlats(json);
    }

    createFilterJson() {
        var json = {
            filters: {
                city: this.props.city,
                subway: this.props.subway,
                address: this.props.address,
                price: this.props.price,
                meters: this.props.meters,
                order: this.props.order,
                rooms: this.props.rooms,
                flatOnPage: this.props.flatOnPage,
                pageNumber: (this.props.pageNumber == 0) ? 1 : this.props.pageNumber
            }
        };
        return (JSON.stringify(json));
    }

    toggleFilters() {
        $('#wrapper').toggleClass('toggled');
    }

    componentWillUnmount() {
        this.props.clearFlats();
    }

    renderFlatType(room, meters) {
    switch (room) {
        case 1:
            return ( <p> Type of the flat: 1 - room flat, {meters} meters </p>);
        case 2:
            return ( <p> Type of the flat: 2 - room flat, {meters} meters </p>);
        case 3:
            return ( <p> Type of the flat: 3 - room flat, {meters} meters </p>);
        case 4:
            return ( <p> Type of the flat: 4 - room flat, {meters} meters </p>);
        case 5:
            return ( <p> Type of the flat: 5 - room flat, {meters} meters </p>);
        case 6:
            return ( <p> Type of the flat: 6 - room flat, {meters} meters </p>);
        case 7:
            return ( <p> Type of the flat: studio, {meters} meters </p>);
        case 8:
            return ( <p> Type of the flat: free plan, {meters} meters </p>);
    }
}
    
    renderFlats() {
        var citySubways = [];
        if (this.props.subways) {
            for (var i = 0; i < this.props.subways.length; i++) {
                if (+this.props.subways[i].cityId === +this.props.city) {
                    for (var j = 0; j < this.props.subways[i].subways.length; j++) {
                        citySubways.push(this.props.subways[i].subways[j].name);
                    }
                    break;
                }
            }
        }
        return this.props.flats.map((flat, i) => {
            return ( 
                <div className = 'flat-panel' key = { flat.id }>
                    <div className = 'col-sm-4' >
                        <div id = { 'carousel' + i } className = 'carousel slide' data-ride = 'carousel'>
                            <ol className = 'carousel-indicators' > { this.renderImagesIcons(flat, i) } </ol>
                            <div className = 'carousel-inner'>
                                { this.renderImages(flat) } 
                            </div>
                            <a className = 'left carousel-control' href = { '#carousel' + i } data-slide = 'prev'>
                                <span className = 'glyphicon glyphicon-chevron-left'></span>
                            </a>
                            <a className = 'right carousel-control' href = { '#carousel' + i } data-slide = 'next'> 
                                <span className = 'glyphicon glyphicon-chevron-right'></span>
                            </a>
                        </div> 
                    </div>
                    <div className = 'col-sm-8'>
                        <p className = 'address' > { flat.city + ', ' + flat.address } </p> 
                        { this.renderFlatType(flat.rooms, flat.squareMeters) }
                        <p> <i className='fa fa-subway' aria-hidden='true'></i> {flat.subway == 0 ? 'No subway chosen' : citySubways[flat.subway - 1] } </p>
                        <br/>
                        <p className = 'price'> Price: { flat.price } rub/month</p>
                        <Link to = { '/flat/' + flat.id }>
                            <button className = 'cd-close more'>More</button>
                        </Link>
                    </div>
                </div>
            );
        });
    }

    renderImages(flat) {
        return flat.photos.map((photo, i) => {
            return ( <div className = { 'item' + (i == 0 ? ' active' : '') } key = { i }>
                        <img className = 'galleryImage' src = { photo } alt = ''/>
                    </div>
            );
        });
    }

    renderImagesIcons(flat, index) {
        return flat.photos.map((photo, i) => {
            return ( <li data-target = { '#carousel' + index } data-slide-to = { i } key = { i } className = { i == 0 ? 'active' : '' }></li>
            );
        });
    }

    render() {
        return ( <div id = 'page-content-wrapper'>
                    <div className = 'container-fluid' style = {{ marginTop: '10px' }}>
                        <div className = 'col-lg-12'>
                            <div className = 'row' >
                                <a href = '#' id = 'menu-toggle' onClick = { this.toggleFilters }>
                                    <i className = 'fa fa-bars fa-2x' ></i>
                                </a>
                            </div>
                            <PriceOrder/>
                            <div className = 'row'>
                                <div className = 'gallery' > 
                                    { this.renderFlats() }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        flats: state.info.flats,
        address: state.info.filters.address,
        city: state.info.filters.city,
        subway: state.info.filters.subway,
        subways: state.info.rusSubways,
        price: state.info.filters.price,
        meters: state.info.filters.meters,
        order: state.info.order,
        rooms: state.info.rooms,
        flatOnPage: state.info.flatOnPage,
        pageNumber: state.info.pageNumber
    };
}

export default connect(mapStateToProps, { getFlats, clearFlats })(FlatsIndex);