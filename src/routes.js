import React from 'react';
import { Route, IndexRoute, Router } from 'react-router';

import App from './containers/App';
import MainIndex from './containers/MainIndex';
import FlatIndex from './containers/FlatIndex';

import { hashHistory } from 'react-router'

// роут для / пути и других
export default (
  <Router path='/' component={ App }>
    <IndexRoute component={ MainIndex }>
	</IndexRoute>
    <Route path='flat/:id' component={ FlatIndex } />
  </Router>
);