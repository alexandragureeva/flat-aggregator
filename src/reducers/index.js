import {combineReducers} from 'redux';
import FlatReducer from './flat_reducer';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers( {
  info: FlatReducer,
  form: formReducer
} );

export default rootReducer;