import { GET_CITIES, GET_FLATS, CHANGE_ORDER, APPLY_FILTER, CHANGE_PAGE, CHANGE_ROOMNUMBER,GET_SUBWAYS, GET_FLAT, CLEAR_FLAT, CLEAR_FLATS } from '../actions/index';
const INITIAL_STATE = {
  flats: [],
  cities: [],
  rusSubways : [],
  filters : {
	  address: "",
	  city: 0,
	  subway: 0,
	  price : [0, 0],
	  meters : [0, 0]
  },
  order: 0,
  rooms : [1, 2, 3, 4, 5, 6, 7, 8],
	pageCount: 10,
	pageNumber: 1,
	flatOnPage: 5,
	flat : null
};

export default function( state = INITIAL_STATE, action )
{
	console.log("action type: " + action.type)
  switch( action.type )
  {
    case GET_CITIES:
      return { ...state, cities: action.payload.data.cities };
	case GET_SUBWAYS:
      return { ...state, rusSubways: action.payload.data.russianSubways };
    case GET_FLATS:
      return { ...state, flats: action.payload.data.flats, pageCount : action.payload.data.pageCount};
	case CHANGE_ORDER:
		return{ ...state, order: action.payload};
	case CHANGE_ROOMNUMBER:
		return{ ...state, rooms: action.payload};
	case APPLY_FILTER:
		return {...state, filters : action.payload};
	case CHANGE_PAGE:
		return {...state, pageNumber : action.payload};
	case GET_FLAT:
		return { ...state, flat: action.payload.data };
	case CLEAR_FLAT:
		return { ...state, flat: null };
	case CLEAR_FLATS:
		return { ...state, flats: [] };
    default:
      return state;
  }
}